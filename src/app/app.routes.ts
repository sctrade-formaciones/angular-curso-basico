import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'create-user', component: CreateUserComponent },
    { path: 'edit-user/:id', component: EditUserComponent },
    { path: '**', pathMatch:'full', redirectTo: '' }
];

export const AppRouting = RouterModule.forRoot(routes);