import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { User } from '../model/user.model';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private users;
  constructor(private http: HttpClient) { }
  
  findAll() {
    return this.http.get('http://localhost:3000/api/users').pipe(map((users: User[]) => {
      this.users = users;
      return users;
    }));
  }

  findById(id: number) {
    return this.users.find((u: User) => u.id === id);
  }

  editUser(user: User) {
    return this.http.put('http://localhost:3000/api/edit', {user}).pipe(map((users: User[]) => {
      this.users = users;
      return users;
    }));
  }

  removeUser(id: number) {
    console.log(id);
    const params: HttpParams = new HttpParams().set('id', String(id));
    return this.http.delete('http://localhost:3000/api/remove', {params}).pipe(map((users: User[]) => {
      this.users = users;
      return users;
    }));
  }

  createUser(user: User) {
    return this.http.post('http://localhost:3000/api/create', {user}).pipe(map((users: User[]) => {
      this.users = users;
      return users;
    }));
  }

}
