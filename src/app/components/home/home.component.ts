import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/model/user.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  users: any[];
  constructor(private _userService: UserService) { }

  ngOnInit() {
    this._userService.findAll().subscribe((users: User[]) => {
      this.users = users;
    });
  }

  remove(id: number) {
    this._userService.removeUser(id).subscribe((users: User[]) => {
      this.users = users;
    });
  }

}
