import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/model/user.model';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  user: User;
  constructor(private route: ActivatedRoute, private _userService: UserService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if(params.id) {
        this.user = this._userService.findById(Number(params.id));
      }
    });
  }

  onSubmit() {
    this._userService.editUser(this.user).subscribe();
  }
}
