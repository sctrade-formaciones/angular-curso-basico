import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/model/user.model';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  formGroup: FormGroup;
  constructor(private _userService: UserService) { }

  ngOnInit() {
    this.formGroup = new FormGroup({
      name: new FormControl('', [Validators.required]),
      age: new FormControl(0, Validators.required),
      adress: new FormControl('', Validators.required),
      phone: new FormControl(0, Validators.required)
    });
  }

  onSubmit() {
    if (this.formGroup.valid) {
      const user: User = {
        'id': 0,
        'name': this.formGroup.value.name,
        'age': this.formGroup.value.age,
        'adress': this.formGroup.value.adress,
        'phone': this.formGroup.value.phone
      };
      this._userService.createUser(user).subscribe();
      
      console.log('user created');
    }
  }

}