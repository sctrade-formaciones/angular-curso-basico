import { AbstractWebDriver } from "protractor/built/browser";

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

let users = [
    {
      id: 1,
      name: 'Peter',
      age: 23,
      adress: 'Calle A 123 piso 4º3',
      phone: 934934959,
      url: 'user.png'
    },
    {
      id: 2,
      name: 'Charles',
      age: 43,
      adress: 'Av Diagonal 320 piso 2º2',
      phone: 935598638,
      url: 'user.png'
    },
    {
      id: 3,
      name: 'John',
      age: 34,
      adress: 'Gran Via CC 198 piso 3º1',
      phone: 938876498,
      url: 'user.png'
    }
  ];
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.route('/api/users')
    .get((req, res) => {
        res.status(200).json(users);
    }).post((req, res) => {
        res.status(200).json(users.find(u => u.id == req.body.id));
    });

app.route('/api/edit').put((req, res) => {
    const user = req.body.user;
    const serverUser = users.find(u => u.id === user.id);
    serverUser.name = user.name;
    serverUser.age = user.age;
    serverUser.adress = user.adress;
    serverUser.phone = user.phone;
    res.status(200).json(users);
});
app.route('/api/create').post((req, res) => {
    const user = req.body.user;
    user.url = 'user.png'
    user.id = Math.max.apply(Math, users.map(u => u.id)) + 1;
    users.push(user);
    res.status(200).json(users);
});
app.route('/api/remove').delete((req, res) => {
    users = users.filter(u => u.id != Number(req.query.id));
    res.status(200).json(users);
});
app.listen(3000, () => {
 console.log('Server running on port 3000');
});
